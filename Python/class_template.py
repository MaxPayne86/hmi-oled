import logging
from time import sleep

### Class definition
class myclass(object):
    def __init__(self):
        logging.basicConfig(filename='log.log', level=logging.DEBUG)
        logging.debug("Method __init__ called")

    def method1(self, param1, param2):
        logging.debug("Method1: %d %d" % (param1, param2))

    def method2(self, param1="Pippo", param2="Pluto"):
        logging.debug("Method1: %s %s" % (string1, string2))

    def run(self):
        while True:
            try:
                sleep(1)
            except KeyboardInterrupt:
                logging.debug("Keyboard interrupt!")
                break
            except IOError:
                logging.error("Error!")
### End class definition

### Main program
if __name__ == '__main__':
    myclass().run()
### End Main program
