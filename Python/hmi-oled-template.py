#!/usr/bin/env python
#
# HMI python source code
#
'''
## License

The MIT License (MIT)

BakeBit: an open source platform for connecting BakeBit Sensors to the NanoPi NEO.
Copyright (C) 2016 FriendlyARM

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
'''
import sys
import subprocess
import threading
import signal
import os
import socket
from time import time, sleep
from enum import Enum
import logging

import bakebit_128_64_oled as oled
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw
from RPi import GPIO

### Global variables
width=128
height=64
mywidth=127
myheight=63
offset_x = 0
offset_y = 8

fontb24 = ImageFont.truetype('DejaVuSansMono-Bold.ttf', 24);
font14 = ImageFont.truetype('DejaVuSansMono.ttf', 14);
smartFont = ImageFont.truetype('DejaVuSansMono-Bold.ttf', 10);
fontb14 = ImageFont.truetype('DejaVuSansMono-Bold.ttf', 14);
font11 = ImageFont.truetype('DejaVuSansMono.ttf', 11);
### End Global variables

### Utility classes definitions
class fsm(Enum):
    HOME = 1
    PAGE1 = 2
    PAGE2 = 3
### End Utility classes definitions

### HMI class definition
class HMI(object):
    def __init__(self):
        logging.basicConfig(filename='hmi.log',level=logging.DEBUG)

        self.fsmState = fsm.HOME

        oled.init() # Initialize display
        oled.setNormalDisplay() # Set display to normal mode (i.e non-inverse mode)
        oled.setHorizontalMode()
        logging.debug("Oled init done")

        self.image = Image.new('1', (width, height))
        self.draw = ImageDraw.Draw(self.image)

        self.lock = threading.Lock()
        self.busy = False

        self.k1 = 0
        self.k2 = 2
        self.k3 = 3

        GPIO.setmode(GPIO.BCM) # GPIO counting mode selection

        GPIO.setup(self.k1, GPIO.IN, pull_up_down=GPIO.PUD_OFF)
        GPIO.setup(self.k2, GPIO.IN, pull_up_down=GPIO.PUD_OFF)
        GPIO.setup(self.k3, GPIO.IN, pull_up_down=GPIO.PUD_OFF)

        signal.signal(signal.SIGTERM, self.__handle_signal)

        GPIO.add_event_detect(self.k1, GPIO.RISING, callback=self.__handle_gpio, bouncetime=50)
        GPIO.add_event_detect(self.k2, GPIO.RISING, callback=self.__handle_gpio, bouncetime=50)
        GPIO.add_event_detect(self.k3, GPIO.RISING, callback=self.__handle_gpio, bouncetime=50)
        logging.debug("Gpio init done")

        self.draw_page()

    def __handle_gpio(self, gpio):
        if self.busy:
            return
        btnStatus = GPIO.input(gpio)
        state = self.fsmState

        if gpio == self.k1:
            logging.debug("K1 pressed")
            if state == fsm.HOME:
                self.update_fsm_state(fsm.HOME)
            elif state == fsm.PAGE1:
                self.update_fsm_state(fsm.HOME)
            elif state == fsm.PAGE2:
                self.update_fsm_state(fsm.PAGE1)
            else:
                self.update_fsm_state(fsm.HOME)

        elif gpio == self.k2:
            logging.debug("K2 pressed")
            if state == fsm.HOME:
                self.update_fsm_state(fsm.PAGE1)
            elif state == fsm.PAGE1:
                self.update_fsm_state(fsm.PAGE2)
            elif state == fsm.PAGE2:
                self.update_fsm_state(fsm.HOME)
            else:
                self.update_fsm_state(fsm.HOME)

        elif gpio == self.k3:
            logging.debug("K3 pressed")

        self.lock.acquire()
        self.busy = True
        self.draw_page()
        self.busy = False
        self.lock.release()

    def __handle_signal(self, signum, stack):
        if signum == signal.SIGTERM:
            oled.clearDisplay()
            exit()

    def draw_page(self):
        state = self.fsmState

        # Draw a black filled box to clear the image.
        self.draw.rectangle((0, 0, width, height), outline=0, fill=0)

        if state==fsm.HOME:
            self.draw.text((0,8), "Title:", font=smartFont, fill=255)

            text1 = "Subtitle 1"
            text2 = "Subtitle 2"

            self.draw.text((0,8+10+2), text1, font=font14, fill=255)
            self.draw.text((0,8+10+1+14+2), text2, font=smartFont, fill=255)

        elif state==fsm.PAGE1:
            self.draw.text((0, 8), 'Page1', font=fontb14, fill=255)

            text1 = "One"
            text2 = "Two"

            self.draw.rectangle((0,8+14,width,8+14+16), outline=0, fill=255)
            self.draw.text((2, 8+14+2), text1, font=font11, fill=0)

            self.draw.rectangle((0,8+14+16,width,8+14+16+16), outline=0, fill=0)
            self.draw.text((2, 8+14+16+2), text2, font=font11, fill=255)

            self.draw.text((0, height-14), '◄', font=fontb14, fill=255)
            self.draw.text(((width/2)-4, height-14), '✓', font=fontb14, fill=255)
            self.draw.text((width-14, height-14), '▼', font=fontb14, fill=255)

        elif state==fsm.PAGE2:
            self.draw.text((0, 8), 'Page2', font=fontb14, fill=255)

            text1 = "Three"
            text2 = "Four"

            self.draw.rectangle((0,8+14,width,8+14+16), outline=0, fill=255)
            self.draw.text((2, 8+14+2), text1, font=font11, fill=0)

            self.draw.rectangle((0,8+14+16,width,8+14+16+16), outline=0, fill=0)
            self.draw.text((2, 8+14+16+2), text2, font=font11, fill=255)

            self.draw.text((0, height-14), '◄', font=fontb14, fill=255)
            self.draw.text(((width/2)-4, height-14), '✓', font=fontb14, fill=255)
            self.draw.text((width-14, height-14), '▼', font=fontb14, fill=255)

        oled.drawImage(self.image)

    def update_fsm_state(self, nextState):
        self.fsmState = nextState

    def run(self):
        while True:
            try:
                sleep(1)
            except KeyboardInterrupt:
                break
            except IOError:
                logging.error("Error!")
### End HMI class definition

### Main program
if __name__ == '__main__':
    HMI().run()
### End Main program
