#!/usr/bin/env python
#
# Aida DSP OS HMI python source code
#
'''
## License

The MIT License (MIT)

BakeBit: an open source platform for connecting BakeBit Sensors to the NanoPi NEO.
Copyright (C) 2016 FriendlyARM

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
'''
import sys
import os
import signal
from time import sleep
import logging

import bakebit_128_64_oled as oled
from PIL import Image
from PIL import ImageFont
from PIL import ImageDraw

### Global variables
width=128
height=64
mywidth=127
myheight=63
offset_x = 0
offset_y = 8

fontb24 = ImageFont.truetype('DejaVuSansMono-Bold.ttf', 24);
font14 = ImageFont.truetype('DejaVuSansMono.ttf', 14);
smartFont = ImageFont.truetype('DejaVuSansMono-Bold.ttf', 10);
fontb14 = ImageFont.truetype('DejaVuSansMono-Bold.ttf', 14);
font11 = ImageFont.truetype('DejaVuSansMono.ttf', 11);
### End Global variables

### HMI class definition
class HMI(object):
    def __init__(self):
        logging.basicConfig(filename='hmi.log',level=logging.DEBUG)

        nparams = len(sys.argv)      
        if nparams>1 and nparams<3:
            self.mode = int(sys.argv[1])
            logging.debug("Mode is %d" % self.mode)
        else:
            logging.error("%s called with wrong parameters number" % sys.argv[0])
            raise ValueError(sys.argv[0]+" called with wrong parameters number")
 
        oled.init() # Initialize display
        oled.setNormalDisplay() # Set display to normal mode (i.e non-inverse mode)
        oled.setHorizontalMode()
        logging.debug("Oled init done")

        self.count = 0;

        # Splash screen
        self.image = Image.open('aida1_logo128x64.png').convert('1')
        oled.drawImage(self.image)
        sleep(0.250)

        self.image = Image.new('1', (width, height))
        self.draw = ImageDraw.Draw(self.image)
        
        signal.signal(signal.SIGTERM, self.__handle_signal)
        
    def __handle_signal(self, signum, stack):
        if signum == signal.SIGTERM:
            logging.debug("Received signal SIGTERM")
            #oled.clearDisplay()
            exit()

    def draw_page(self, string):
        # Draw a black filled box to clear the image.            
        self.draw.rectangle((0, 0, width, height), outline=0, fill=0)

        # Write things on the display according to selected mode
        if self.mode == 0:
            text = "MODE1"
            text1 = "text1"
        elif self.mode == 1:
            text = "MODE2"
            text1 = "text2"
        elif self.mode == 2:
            text = "MODE3"
            text1 = "text3"
        elif self.mode == 10:
            text = "MODE10"
            text1 = "text10"
        else:
            text = "Error"
            text1 = "unknown" 

        self.draw.text((0,8), text, font=fontb24, fill=255)
        self.draw.text((0,8+24+2), text1, font=font14, fill=255)
        text2 = string
        self.draw.text((0,8+24+2+14+2), text2, font=font14, fill=255)

        oled.drawImage(self.image)

    def run(self):
        while True:
            try:
                self.count = self.count + 1
                if self.count == 1:
                    string = "mode."
                elif self.count == 2:
                    string = "mode.."
                elif self.count == 3:
                    string = "mode..."
                    self.count = 0

                self.draw_page(string)
                sleep(1.5)
            except KeyboardInterrupt:
                break
            except IOError:                                                                              
                logging.error("Error!")
### End HMI class definition

### Main program
if __name__ == '__main__':
    HMI().run()
### End Main program
