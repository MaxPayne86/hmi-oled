# Example to use the three buttons available in Aida DSP OS
# hardware v1.0
# Print button status when pressed

from sys import exit
from time import sleep

from RPi import GPIO

class GpioExample(object):
    def __init__(self):
        self.k1 = 0
        self.k2 = 2
        self.k3 = 3
        
        GPIO.setmode(GPIO.BCM) # GPIO counting mode selection

        GPIO.setup(self.k1, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.setup(self.k2, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        GPIO.setup(self.k3, GPIO.IN, pull_up_down=GPIO.PUD_UP)

        GPIO.add_event_detect(self.k1, GPIO.FALLING, callback=self.__handle_gpio, bouncetime=50)
        GPIO.add_event_detect(self.k2, GPIO.FALLING, callback=self.__handle_gpio, bouncetime=50)
        GPIO.add_event_detect(self.k3, GPIO.FALLING, callback=self.__handle_gpio, bouncetime=50)
    
    def __handle_gpio(self, gpio):
        btnStatus = GPIO.input(gpio)
        print("Gpio %d pressed! Value = %d" % (gpio, btnStatus))

    def run(self):
        while True:
            try:
                sleep(1)
            except KeyboardInterrupt:
                print("\nBye!")
                exit()

if __name__ == '__main__':
    GpioExample().run()

