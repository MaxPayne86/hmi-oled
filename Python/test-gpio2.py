# Example to use the three buttons available in Aida DSP OS
# hardware v1.0
# Measure the pressing time of k2

from sys import exit
from time import time, sleep

from RPi import GPIO

class GpioExample(object):
    def __init__(self):
        self.k2 = 2
        self.k2_lock = False
        self.k2_startTime = 0.0
        self.k2_deltaTime = 0.0
        
        GPIO.setmode(GPIO.BCM) # GPIO counting mode selection

        GPIO.setup(self.k2, GPIO.IN, pull_up_down=GPIO.PUD_UP)

        GPIO.add_event_detect(self.k2, GPIO.BOTH, callback=self.__handle_gpio) # Do not pass bouncetime to obtain faster response?
    
    def __handle_gpio(self, gpio):
        if (gpio == self.k2):
            if (self.k2_lock == False):
                self.k2_startTime = time()
                self.k2_lock = True
            else:
                self.k2_deltaTime = time() - self.k2_startTime
                self.k2_lock = False
                print("Gpio %d deltaTime: %.02f[s]" % (gpio, self.k2_deltaTime))

    def run(self):
        while True:
            try:
                sleep(1)
            except KeyboardInterrupt:
                print("\nBye!")
                exit()

if __name__ == '__main__':
    GpioExample().run()

