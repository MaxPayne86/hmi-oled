# Program to test OLED display On/Off

from time import time, sleep
import smbus

bus=smbus.SMBus(0)
address=0x3C

cmd=0x00 # Command_Mode
byte1=0xAE # Display_Off_Cmd
byte2=0xAF # Display_On_Cmd

def sendCommand(byte):
    try:
        block=[]
        block.append(byte)
        return bus.write_i2c_block_data(address,cmd,block)
    except IOError:
        print("IOError")
        return -1

sendCommand(byte1)
sleep(5)
sendCommand(byte2)

bus.close()
