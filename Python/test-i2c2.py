# Program to test OLED display brightness

from time import time, sleep
import smbus
import sys

if len(sys.argv) != 2:
    print("Missing contrast 0-255 argument")
    exit()

bus=smbus.SMBus(0)
address=0x3C

cmd=0x00 # Command_Mode
byte1=0x81 # Set_Brightness_Cmd
byte2=int(sys.argv[1]) # Brightness level 0-255
print(byte2)

def sendCommand(byte):
    try:
        block=[]
        block.append(byte)
        return bus.write_i2c_block_data(address,cmd,block)
    except IOError:
        print("IOError")
        return -1

sendCommand(byte1)
sendCommand(byte2)

bus.close()
